import java.util.Scanner;

/**
 * Created by Jeannius on 10/22/2015.
 */
public class Ackermann {




    public static void main(String[] args){

        long firstNumber = getUserInput();
        System.out.println("Great! ");
        long secondNumber = getUserInput();
        System.out.printf("First number: %d \nSecond number: %d\n\n", firstNumber,secondNumber);
        System.out.println("Begining Ackermann Calculations");
        long result = ackermannFunction(firstNumber, secondNumber);
        System.out.printf("Result is: %d",result );
    }

    private static int getUserInput(){
        int number;
        String s="";
        do {
            Scanner reader = new Scanner(System.in);
            System.out.println(s);
            System.out.println("Please enter a positive number: ");
            number = reader.nextInt();
            s ="You've entered a negative number!";
        }while (!numberIsPositive(number));
        return number;
    }

    private static boolean numberIsPositive(int number){
        if(number>=0) return true;
        else return false;
    }

    public static long ackermannFunction(long x,long y){
        if(x==0) return y+1;
        else if(y==0) return ackermannFunction(x-1, 1);
        else return ackermannFunction(x-1, ackermannFunction(x,y-1));
    }


}
